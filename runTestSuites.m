% Script de lancement des tests unitaire avec coverage

import matlab.unittest.plugins.CodeCoveragePlugin
import matlab.unittest.TestRunner
import matlab.unittest.plugins.codecoverage.CoverageReport

root = fileparts(which("runTestSuites"));
cd(root);

test_list = dir("./TEST_SUITES/**/TEST_SUITE*.m");
if isempty(test_list)
  fprintf("Aucun fichier de test\n");
else
  tested_files = {};
  test_files = {};
  for i=1:size(test_list, 1)
    tested_files{i} = which(test_list(i).name(12:end-2));
    test_files{i} = which(test_list(i).name);
  end

  reportFolder = [root '/coverage'];
  reportFormat = CoverageReport(reportFolder);

  plugin = CodeCoveragePlugin.forFile(tested_files, 'Producing', reportFormat);

  suite = testsuite(test_files);
  runner = TestRunner.withTextOutput;
  runner.addPlugin(plugin);

  runner.run(suite);
end