classdef MyClassTested

    methods(Access = public)
        function obj = MyClassTested(obj)
        
        end

        function methodPublic(obj)
            fprintf("methodPublic running\n");
        end
    end

    methods (Access = {?MyClassTested, ?TEST_SUITE_MyClassTested})
        function methodProtected(obj)
            fprintf("methodProtected running\n");
        end
    end

end
