function genererTestSuiteTemplate(classOrFunction)

%% Init
root = fileparts(which('genererTestSuiteTemplate'));
cd(root);

%% Verification de l'existance de l'objet
if isempty(which(classOrFunction))
  fprintf("%s introuvable, annulation...\n", classOrFunction);
  return;
end

%% Verification de la presence d'un test pour ne pas l'ecraser
if exist(sprintf("TEST_SUITES/TEST_SUITE_%s", classOrFunction), "dir")
  fprintf("Dossier de test existant, annulation...\n");
  return;
end

%% Verification du type d'objet (classe ou fonction)
type = getTypeOfObject(classOrFunction);

%% Ecriture des fichier template de test
if strcmp(type, 'function')
  % generer le fichier de test
  generatedFolderPath = sprintf("TEST_SUITES/TEST_SUITE_%s", classOrFunction);
  mkdir(generatedFolderPath);
  fid = fopen(sprintf("%s/TEST_SUITE_%s.m", generatedFolderPath, classOrFunction), 'w');
  if fid < 0
    fprintf("Impossible de generer le fichier de test, annulation\n");
    return;
  end

  writeFunctionTemplate(fid, classOrFunction);

  addpath(genpath(generatedFolderPath));

elseif strcmp(type, 'class')
  % generer le fichier de test
  generatedFolderPath = sprintf("TEST_SUITES/TEST_SUITE_%s", classOrFunction);
  mkdir(generatedFolderPath);
  fid = fopen(sprintf("%s/TEST_SUITE_%s.m", generatedFolderPath, classOrFunction), 'w');
  if fid < 0
    fprintf("Impossible de generer le fichier de test, annulation\n");
    return;
  end

  writeClassTemplate(fid, classOrFunction)

  addpath(genpath(generatedFolderPath));

end




%% Helpers
  function type = getTypeOfObject(classOrFunction)
    % Verifie la presence d'un classdef ou d'un function au debut du
    % fichier
    type = 'none';
    fid = fopen(which(classOrFunction), 'r');
    if fid < 0
      fprintf("Erreur lors de la detection de type d'objet, fichier introuvable\n");
      return;
    end

    line = fgetl(fid);
    while strcmp(line(1), '%') && ~feof(fid)
      line = fgetl(fid);
    end
    
    % Si on a que des commentaires
    if feof(fid)
      fprintf("Erreur lors de la detection de type d'objet, pas de ligne exploitable a tester\n");
      return;
    end

    if contains(line, 'function ')
      type = "function";
    elseif contains(line, 'classdef ')
      type = "class";
    else
      fprintf("Erreur lors de la detection de type d'objet, objet non reconnu\n");
      return;
    end
  end

  function writeFunctionTemplate(fid, name)
    % Construit la classe de test sur fonction
    tab_char = '  ';
    double_tab_char = [tab_char tab_char];

    % Entete classe de test
    fprintf(fid, "classdef TEST_SUITE_%s < matlab.unittest.TestCase\n", name);
    
    % Block properties
    fprintf(fid, "%sproperties\n", tab_char);
    fprintf(fid, "%s%%Declarer ici toutes les variables communes aux cas de test\n", double_tab_char);
    fprintf(fid, "%send\n", tab_char);

    % Block method setup
    fprintf(fid, "%smethods(TestClassSetup)\n", tab_char);
    fprintf(fid, "%s%%Declarer ici les fonctions executee a l'instanciation de la classe\n", double_tab_char);
    fprintf(fid, "%s%%Ex:\n", double_tab_char);
    fprintf(fid, "%s%%function classSetup(testCase)\n", double_tab_char);
    fprintf(fid, "%s%%  testCase.attribut1 = 0;\n", double_tab_char);
    fprintf(fid, "%s%%  testCase.attribut2 = 'OK';\n", double_tab_char);
    fprintf(fid, "%s%%end\n", double_tab_char);
    fprintf(fid, "%send\n", tab_char);

    % Block method Test
    fprintf(fid, "%smethods(Test)\n", tab_char);
    fprintf(fid, "%s%%Declarer ici les fonctions de test\n", double_tab_char);
    fprintf(fid, "%s%%Ex:\n", double_tab_char);
    fprintf(fid, "%s%%function testCase1(testCase)\n", double_tab_char);
    fprintf(fid, "%s%%  testCase.verifyEqual(testCase.attribut1, testCase.attribut2);\n", double_tab_char);
    fprintf(fid, "%s%%end\n", double_tab_char);
    fprintf(fid, "%send\n", tab_char);


    
    % Fin de fichier
    fprintf(fid, "end\n");

  end

  function writeClassTemplate(fid, name)
    % Construit la classe de test sur classe
    tab_char = '  ';
    double_tab_char = [tab_char tab_char];

    % Entete classe de test
    fprintf(fid, "classdef TEST_SUITE_%s < matlab.unittest.TestCase\n", name);

    % Block de commentaire d'usage 
    fprintf(fid, "%s%%Pour que la classe testee soit compatible du test unitaire, \n", tab_char);
    fprintf(fid, "%s%%le block 'methods' contenant les fonctions a tester\n", tab_char);
    fprintf(fid, "%s%%autorise l'acces a la classe de test. Il faut rajouter : \n", tab_char);
    fprintf(fid, "%s%%""methods (Access = {?MyClassTested, ?TEST_SUITE_MyClassTested})\n", tab_char);
    
    % Block properties
    fprintf(fid, "%sproperties\n", tab_char);
    fprintf(fid, "%s%%Declarer ici toutes les variables communes aux cas de test\n", double_tab_char);
    fprintf(fid, "%s%s;\n", double_tab_char, lower(name));
    fprintf(fid, "%send\n", tab_char);

    % Block method setup
    fprintf(fid, "%smethods(TestClassSetup)\n", tab_char);
    fprintf(fid, "%s%%Declarer ici les fonctions executee a l'instanciation de la classe\n", double_tab_char);
    fprintf(fid, "%sfunction classSetup(testCase)\n", double_tab_char);
    fprintf(fid, "%s  testCase.%s = %s();\n", double_tab_char, lower(name), name);
    fprintf(fid, "%send\n", double_tab_char);
    fprintf(fid, "%send\n", tab_char);

    % Block method Test
    fprintf(fid, "%smethods(Test)\n", tab_char);
    fprintf(fid, "%s%%Declarer ici les fonctions de test\n", double_tab_char);
    fprintf(fid, "%s%%Ex:\n", double_tab_char);
    fprintf(fid, "%s%%function testCase1(testCase)\n", double_tab_char);
    fprintf(fid, "%s%%  testCase.verifyEqual(testCase.attribut1, testCase.attribut2);\n", double_tab_char);
    fprintf(fid, "%s%%end\n", double_tab_char);
    fprintf(fid, "%send\n", tab_char);


    
    % Fin de fichier
    fprintf(fid, "end\n");
  end
end