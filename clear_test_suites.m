% Script de nettoyage total des tests

root = fileparts(which('runTestSuites.m'));
cd(root);

!rm -rf TEST_SUITES/* coverage/
